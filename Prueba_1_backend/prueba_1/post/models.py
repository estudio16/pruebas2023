from django.db import models

class post(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()

class Auto(models.Model):
    anno = models.DateField()
    marca = models.CharField(max_length=15)
    color = models.CharField(max_length=8)
    combustible = models.CharField(max_length=15)
    numPuertas = models.IntegerField()
    traccion = models.CharField(max_length=10)