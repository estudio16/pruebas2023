from django.contrib import admin
from post.models import post, Auto

@admin.register(post)
class PostAdmin(admin.ModelAdmin):
    lis_display = ['id','title']

@admin.register(Auto)
class AutoAdmin(admin.ModelAdmin):
    lis_display = ['id','anno','marca','color','combustible','numPuertas','traccion']
