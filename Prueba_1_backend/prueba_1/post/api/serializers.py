from rest_framework.serializers import ModelSerializer
from post.models import post, Auto
class PostSerializer(ModelSerializer):
    class Meta:
        model = post
        fields = ['id','title','content']

class AutoSerializer(ModelSerializer):
    class Meta:
        model = Auto
        fields = ['id','anno','marca','color','combustible','numPuertas','traccion']

