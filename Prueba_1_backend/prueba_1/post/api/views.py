from rest_framework.viewsets import ModelViewSet
from post.models import post, Auto
from post.api.serializers import PostSerializer,AutoSerializer

class PostApiViewSet(ModelViewSet):
    serializer_class = PostSerializer
    queryset = post.objects.all()
    
class AutoApiViewset(ModelViewSet):
    serializer_class = AutoSerializer
    queryset = Auto.objects.all()