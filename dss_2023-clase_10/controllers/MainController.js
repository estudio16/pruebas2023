const { request, response } = require("express")
const utils = require("../resources/utils")

const recibe_datos = (request,response) =>{
    response.render('recibe_datos',{locals: {busqueda : utils.validaSanitizer(request.body.buscar)}})
}
const envia_datos = (request,response) =>{
    response.render('envia_datos')
}

module.exports = {
    envia_datos,
    recibe_datos,
}