from django.apps import AppConfig


class Prueba1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'prueba_1'

class PostConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'post'