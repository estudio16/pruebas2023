const express = require('express')
const router = express.Router()
const main = require('../controllers/MainController')

router.get('/envia_datos',main.envia_datos)
router.post('/recibe_datos',main.recibe_datos)

module.exports = router